extern crate specs;
extern crate specs_guided_join;

use specs::{
    Builder, Component, DispatcherBuilder, Entity, System, VecStorage, World, WriteStorage,
};
use specs_guided_join::GuidedJoin;

#[derive(Debug, PartialEq)]
struct Pos(f32);

impl Component for Pos {
    type Storage = VecStorage<Self>;
}

struct SysA {
    guide: Vec<Entity>,
}

impl SysA {
    #[inline(always)]
    pub fn new(guide: Vec<Entity>) -> Self {
        SysA { guide: guide }
    }
}

impl<'a> System<'a> for SysA {
    type SystemData = (WriteStorage<'a, Pos>);

    #[inline]
    fn run(&mut self, mut pos: Self::SystemData) {
        for pos in (&mut pos).guided_join(&self.guide) {
            pos.0 += 1.0;
        }
    }
}

#[test]
fn test_guided_join() {
    let mut world = World::new();

    world.register::<Pos>();

    let entity0 = world.create_entity().with(Pos(0.0)).build();
    let entity1 = world.create_entity().with(Pos(1.0)).build();
    let entity2 = world.create_entity().with(Pos(2.0)).build();

    let guide = vec![entity2, entity0, entity1];

    let mut dispatcher = DispatcherBuilder::new()
        .with(SysA::new(guide.clone()), "sys_a", &[])
        .build();

    dispatcher.dispatch(&mut world.res);

    let read_pos = world.read_storage::<Pos>();
    let entities: Vec<&Pos> = (&read_pos).guided_join(&guide).collect();

    assert_eq!(&*entities, [&Pos(3.0), &Pos(1.0), &Pos(2.0)]);
}
