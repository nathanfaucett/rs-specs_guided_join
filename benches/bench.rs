#![feature(test)]

extern crate test;

extern crate specs;
extern crate specs_guided_join;

use test::Bencher;

use specs::{
    Builder, Component, DispatcherBuilder, Entity, Join, System, VecStorage, World, WriteStorage,
};
use specs_guided_join::GuidedJoin;

#[derive(Debug, PartialEq)]
struct Pos(f32);

impl Component for Pos {
    type Storage = VecStorage<Self>;
}

struct SysGuidedJoin {
    guide: Vec<Entity>,
}

impl SysGuidedJoin {
    #[inline(always)]
    pub fn new(guide: Vec<Entity>) -> Self {
        SysGuidedJoin { guide: guide }
    }
}

impl<'a> System<'a> for SysGuidedJoin {
    type SystemData = (WriteStorage<'a, Pos>);

    #[inline]
    fn run(&mut self, mut pos: Self::SystemData) {
        for pos in (&mut pos).guided_join(&self.guide) {
            pos.0 += 1.0;
        }
    }
}

struct SysJoin;

impl SysJoin {
    #[inline(always)]
    pub fn new() -> Self {
        SysJoin
    }
}

impl<'a> System<'a> for SysJoin {
    type SystemData = (WriteStorage<'a, Pos>);

    #[inline]
    fn run(&mut self, mut pos: Self::SystemData) {
        for pos in (&mut pos).join() {
            pos.0 += 1.0;
        }
    }
}

#[bench]
fn bench_specs_guided_join(b: &mut Bencher) {
    let mut world = World::new();

    world.register::<Pos>();

    let entity0 = world.create_entity().with(Pos(0.0)).build();
    let entity1 = world.create_entity().with(Pos(1.0)).build();
    let entity2 = world.create_entity().with(Pos(2.0)).build();

    let guide = vec![entity2, entity0, entity1];

    let mut dispatcher = DispatcherBuilder::new()
        .with(SysGuidedJoin::new(guide), "sys_guided_join", &[])
        .build();

    b.iter(move || {
        dispatcher.dispatch(&mut world.res);
    });
}

#[bench]
fn bench_specs_join(b: &mut Bencher) {
    let mut world = World::new();

    world.register::<Pos>();

    let _ = world.create_entity().with(Pos(0.0)).build();
    let _ = world.create_entity().with(Pos(1.0)).build();
    let _ = world.create_entity().with(Pos(2.0)).build();

    let mut dispatcher = DispatcherBuilder::new()
        .with(SysJoin::new(), "sys_join", &[])
        .build();

    b.iter(move || {
        dispatcher.dispatch(&mut world.res);
    });
}
