# specs_guided_join

specs guided join for ordered join iter

```toml
specs_guided_join = "0.2"
```

```rust
extern crate specs_guided_join;

use specs_guided_join::GuidedJoin;


let guide = vec![entity1, entity0, entity2];

for component in (&mut components).guided_join(&guide) {
    // ...
}
```
